> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Kevin Berry

### p2 Requirements:

*Three Parts*

1. cd to webapps subdirectory: Example (windows): cd C:/tomcat/webapps
2. Clone assignment starter files: git clone https://bitbucket.org/mjowett/p2_student_files p2 (clones p2_student_files Bitbucket repo into p2 local directory)
a. Review subdirectories and files, especially META-INF and WEB-INF
b. Each assignment *must* have its own global subdirectory containing nav.jsp.

3. NB: Because of the way the newer version of Tomcat recognizes web applications (as well as associated include files), *all* of the assignments will need to be placed right under webapps.

1. Create repositories for lis4368p2
2. Push project to bitbucket

#### README.md file should include the following items:

* Deliverables: 

1. Provide Bitbucket read-only access to p2 repo, *must* include README.md, using Markdown syntax.
2. Blackboard Links: p2 Bitbucket repo


> This is a blockquote.
>
> This is the second paragraph in the blockquote.
>

#### Project Screenshots:

*Screenshot of p2 *:

![Validation: insert](img/insert.png)
![Validation: passed](img/success.png)
![Validation: passed](img/results.png)
![Validation: passed](img/update.png)
![Validation: passed](img/updatemysql.png)
![Validation: passed](img/delete.png)
![Validation: mysql](img/deletemysql.png)


